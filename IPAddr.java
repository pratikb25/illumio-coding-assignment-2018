
class IPAddr {
	long ipInt;

	private static long convert(long a, long b, long c, long d) {
		long p1=(a << 24);
		long p2=(b << 16);
		long p3=(c << 8);
		long p4=d;
		return (p1+p2+p3+p4);
	}

	public IPAddr(long a, long b, long c, long d) {
		this.ipInt = IPAddr.convert(a,b,c,d);
	}

	public boolean matches(IPAddr ip) {
		if(this.ipInt != ip.ipInt)
			return false;
		return  true;
	}
}