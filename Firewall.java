import java.io.*;
import java.util.*;

class Firewall {
	ArrayList<Rule>rules;

	public Firewall(String file) {
		rules = new ArrayList<Rule>();
		String line = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine();
			while(line != null) {
				String [] columns = line.split(",");
				Rule r = new Rule(columns[0], columns[1], columns[2], columns[3]);
				rules.add(r);
				line = br.readLine();
			}
			br.close();
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

	private boolean equals(Rule r1, Rule r2) {
		if(r1.direction != r2.direction)
			return false;
		if(r1.protocol != r2.protocol)
			return false;

		int port = r1.ports[0];
		if(r2.ports.length == 1) { 
			if(r2.ports[0] != port)
				return false;
		}
		else if(r2.ports.length == 2) {
			if(!(port >= r2.ports[0] && port <= r2.ports[1]))
				return false;
		}
		else
			return false;

		IPAddr ip = r1.ips[0];
		if(r2.ips.length == 1) { 
			if(r2.ips[0].ipInt != ip.ipInt) {
				return false;
			}
		}
		else if(r2.ips.length == 2) {
			long l = r2.ips[0].ipInt, u = r2.ips[1].ipInt;
			long n = r1.ips[0].ipInt;
			if(!(n >= l && n <= u)) {
				return false;
			}
		}
		else
			return false;

		return true;
	}

	void display(){
		for(Rule r:this.rules)
			System.out.println(r);
	}

	public boolean accept_packets(String d, String p, int port, String ip) {
		Rule r = new Rule(d, p, Integer.toString(port), ip);
		boolean found = false;
		for(Rule r1 : this.rules) {
			if(equals(r, r1))
				return true;
		}
		return false;
	}
}