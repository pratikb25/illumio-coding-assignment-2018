# Illumio Coding Assignment 2018 (Solution)
## Language used: Java
## Testing:
    - Tested the code by providing different input sets provided in the doc file
    - Used the same CSV file provided in the problem description
    - Used Java8 for development and testing

## Design details:
    - Designed separate custom classes for a Rule-specification and IPv4 address
    - Implemented all of the four fields of a rule as primitive values to improve performance in case of huge input set.
    - Used enums to define all possible directions and protocol values so that adding more values to them in future will require less changes.
    - Caveat: Declared the fields of a rule as "default" visibility in order to enable easy and quick access to their values using dot '.' operator although access to them can be restricted with a set of getter and setter methods

## Areas of interest:
   - All the areas mentioned in the document are very promising and exciting to work in. Based on what I learned about Illumio, I have done similar sort of work at my last job. I am sure my prior experience would leverage my performance and I would get to learn a lot of new things from each team. Based on my prior history and experience, I would like to be considered for following teams in following order:
      - (1) Data team
      - (2) Platform team and
      - (3) Policy team
